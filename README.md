# Test-case NetBeans Maven dependency versions

Test-case to show a bug in NetBeans 12, on displaying the Maven dependency versions.

See https://issues.apache.org/jira/browse/NETBEANS-4656